#include <iostream>

using namespace std;

void drawGrid(int grid[][7]) { //draw a table
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 7; j++) {
			if (grid[i][j] == 0) {
				cout << "| ";
			}
			else if (grid[i][j] == 1) {
				cout << "|x";
			}
			else if (grid[i][j] == 2) {
				cout << "|o";
			}
		}
		cout << endl;
	}

	cout << ":1:2:3:4:5:6:\n";
}

void playerDrop(int turn, int grid[][7]) { //player choose position to drop x
	int x;
	int col1 = 5;
	int col2 = 5;
	int col3 = 5;
	int col4 = 5;
	int col5 = 5;
	int col6 = 5;
	int col7 = 5;

	cout << "\nchoose hole to drop down 'x' ";
	cin >> x;
	x = x - 1;

	switch (x) {
	case 0:
		if (grid[col1][0] == 0) {
			grid[col1][0] = turn;
		}
		else if (grid[col1][0] != 0) {
			while (grid[col1][0] != 0 && col1 >= 0) {
				col1--;
			}
			grid[col1][0] = turn;
		}
		break;
	case 1:
		if (grid[col2][1] == 0) {
			grid[col2][1] = turn;
		}
		else if (grid[col2][1] != 0) {
			while (grid[col2][1] != 0 && col2 >= 0) {
				col2--;
			}
			grid[col2][1] = turn;
		}
		break;

	case 2:
		if (grid[col3][2] == 0) {
			grid[col3][2] = turn;
		}
		else if (grid[col3][2] != 0) {
			while (grid[col3][2] != 0 && col3 >= 0) {
				col3--;
			}
			grid[col3][2] = turn;
		}
		break;

	case 3:
		if (grid[col4][3] == 0) {
			grid[col4][3] = turn;
		}
		else if (grid[col4][3] != 0) {
			while (grid[col4][3] != 0 && col4 >= 0) {
				col4--;
			}
			grid[col4][3] = turn;
		}
		break;

	case 4:
		if (grid[col5][4] == 0) {
			grid[col5][4] = turn;
		}
		else if (grid[col5][4] != 0) {
			while (grid[col5][4] != 0 && col5 >= 0) {
				col5--;
			}
			grid[col5][4] = turn;
		}
		break;

	case 5:
		if (grid[col6][5] == 0) {
			grid[col6][5] = turn;
		}
		else if (grid[col6][5] != 0) {
			while (grid[col6][5] != 0 && col6 >= 0) {
				col6--;
			}
			grid[col6][5] = turn;
		}
		break;

	case 6:
		if (grid[col7][6] == 0) {
			grid[col7][6] = turn;
		}
		else if (grid[col7][6] != 0) {
			while (grid[col7][6] != 0 && col7 >= 0) {
				col7--;
			}
			grid[col7][6] = turn;
		}
		break;
	}
}

void checkWin(int grid[][7], int& chkWin) {
	for (int i = 3; i < 6; i++) {
		for (int j = 0; j < 7; j++) {
			if (grid[i][j] == 1
				&& grid[i][j] == grid[i - 1][j]
				&& grid[i][j] == grid[i - 2][j]
				&& grid[i][j] == grid[i - 3][j]) {
				chkWin = 1;
			}
			else if (grid[i][j] == 2
				&& grid[i][j] == grid[i - 1][j]
				&& grid[i][j] == grid[i - 2][j]
				&& grid[i][j] == grid[i - 3][j]) {
				chkWin = 2;
			}
		}

	}
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 4; j++) {
			if (grid[i][j] == 1
				&& grid[i][j] == grid[i][j + 1]
				&& grid[i][j] == grid[i][j + 2]
				&& grid[i][j] == grid[i][j + 3]) {
				chkWin = 1;
			}
			else if (grid[i][j] == 2
				&& grid[i][j] == grid[i][j + 1]
				&& grid[i][j] == grid[i][j + 2]
				&& grid[i][j] == grid[i][j + 3]) {
				chkWin = 2;
			}
		}
	}
	for (int i = 4; i < 6; i++) {
		for (int j = 0; j < 4; j++) {
			if (grid[i][j] == 1
				&& grid[i][j] == grid[i - 1][j + 1]
				&& grid[i][j] == grid[i - 2][j + 2]
				&& grid[i][j] == grid[i - 3][j + 3]) {
				chkWin = 1;
			}
			else if (grid[i][j] == 2
				&& grid[i][j] == grid[i - 1][j + 1]
				&& grid[i][j] == grid[i - 2][j + 2]
				&& grid[i][j] == grid[i - 3][j + 3]) {
				chkWin = 2;
			}
		}
	}
	for (int i = 4; i < 6; i++) {
		for (int j = 3; j < 7; j++) {
			if (grid[i][j] == 1
				&& grid[i][j] == grid[i - 1][j - 1]
				&& grid[i][j] == grid[i - 2][j - 2]
				&& grid[i][j] == grid[i - 3][j - 3]) {
				chkWin = 1;
			}
			else if (grid[i][j] == 2
				&& grid[i][j] == grid[i - 1][j - 1]
				&& grid[i][j] == grid[i - 2][j - 2]
				&& grid[i][j] == grid[i - 3][j - 3]) {
				chkWin = 2;
			}
		}
	}

}

void AIdrop(int grid[6][7]) { //AI turn
	int win = 0;
	bool AIplay = false;
	for (int a = 0; a < 6; a++) {
		for (int b = 0; b < 7; b++) {
			if (AIplay == false) {
				if (grid[a][b] == 0) {
					grid[a][b] = 1;
					checkWin(grid, win);
					if (win == 1) {
						grid[a][b] = 2; //mark this cell if user will win
						AIplay = true;
					}
					else {
						grid[a][b] = 0;
					}
				}
			}
		}
	}

	if (AIplay == false) {
		int col1 = 5;
		int col2 = 5;
		int col3 = 5;
		int col4 = 5;
		int col5 = 5;
		int col6 = 5;
		int col7 = 5;

		for (int a = 1; a <= 7; a++) {
			if (AIplay == true) {
				break;
			}
			switch (a) {
			case 0:
				if (grid[col1][0] == 0) {
				grid[col1][0] = 2;
				}
				else if (grid[col1][0] != 0) {
					while (grid[col1][0] != 0 && col1 >= 0) {
						col1--;
					}
					grid[col1][0] = 2;

				}
				break;

			case 1:
				if (grid[col2][1] == 0) {
					grid[col2][1] = 2;
				}
				else if (grid[col2][1] != 0) {
					while (grid[col2][1] != 0 && col2 >= 0) {
						col2--;
					}
					grid[col2][1] = 2;

				}
				AIplay = true;
				break;

			case 2:
				if (grid[col3][2] == 0) {
					grid[col3][2] = 2;
				}
				else if (grid[col3][2] != 0) {
					while (grid[col3][2] != 0 && col3 >= 0) {
						col3--;
					}
					grid[col3][2] = 2;

				}
				AIplay = true;
				break;

			case 3:
				if (grid[col4][3] == 0) {
					grid[col4][3] = 2;
				}
				else if (grid[col4][3] != 0) {
					while (grid[col4][3] != 0 && col4 >= 0) {
						col4--;
					}
					grid[col4][3] = 2;

				}
				AIplay = true;
				break;

			case 4:
				if (grid[col5][4] == 0) {
					grid[col5][4] = 2;
				}
				else if (grid[col5][4] != 0) {
					while (grid[col5][4] != 0 && col5 >= 0) {
						col5--;
					}
					grid[col5][4] = 2;

				}
				AIplay = true;
				break;

			case 5:
				if (grid[col6][5] == 0) {
					grid[col6][5] = 2;
				}
				else if (grid[col6][5] != 0) {
					while (grid[col6][5] != 0 && col6 >= 0) {
						col6--;
					}
					grid[col6][5] = 2;

				}
				AIplay = true;
				break;

			case 6:
				if (grid[col7][6] == 0) {
					grid[col7][6] = 2;
				}
				else if (grid[col7][6] != 0) {
					while (grid[col7][6] != 0 && col7 >= 0) {
						col7--;
					}
					grid[col7][6] = 2;

				}
				AIplay = true;
				break;
			}
		}
	}
	cout << "\nAI played\n";
}

void switchTurn(int& turn) { //switch between AI, player
	if (turn == 1) { //user turn

		turn = 2;
	}
	else if (turn == 2) { //A.I. turn
		turn = 1;
	}

}