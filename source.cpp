#include <iostream>
#include "connect4.h";

using namespace std;

int main() {
	int grid[6][7] = {}; //grid table for connect4
	int turn = 1;
	int win = 0;
	int num;
	while (win == 0) {
		drawGrid(grid);
		cout << endl;
		if (turn == 1) { //User turn
			playerDrop(turn, grid);
		}
		if (turn == 2) { //A.I. turn
			AIdrop(grid);
		}
		checkWin(grid, win);
		if (win != 0) {
			break;
		}
		switchTurn(turn);
		cout << endl;
	}

	drawGrid(grid);
	if (win == 0) { //show who win
		cout << "Player, AI draw!";
	}
	if (win == 1) {
		cout << "Player win!";
	}
	if (win == 2) {
		cout << "Player lose!";
	}


	return 0;
}

